---
layout: handbook-page-toc
title: Alliance Partners
description: "Support specific information for alliance partners"
---

Also known as Technology Partners, Alliance Partners are looking to integrate
with our solution. Through product integrations, GitLab helps developers
compile all their work into one tool that can be accessed anywhere. We work
closely through partnerships to provide developers with a single DevOps
experience.

## Contacting Support

Alliance Partners may contact us by submitting a ticket from the
[support portal](https://support.gitlab.com). Each representative of an
Alliance Partner must arrange for their account on the
[support portal](https://support.gitlab.com) to be created for  them
**prior to submitting a ticket for the first time**. To make that
arrangment, they should contact their Technical Account Manager, Account
Executive or other member of their GitLab Sales team.

Once the account has been created, an Alliance Partner should submit tickets
using only 
[this specialized form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360001172559).
Tickets submitted using any other form will not route correctly, which will
likely result in delays.

**Note to Support**: Pay close attention to the organization notes an Alliance
Partner has on their account. These often provide critical information about how
to provide the very best support possible.

## File uploads

When Alliance Partners need to send files to GitLab Support, we have 3
methods available to them:

* Standard ticket uploads (20MB max)
* [Support Uploader](https://about.gitlab.com/support/providing-large-files.html#support-uploader)
* A specialized connection to an s3 bucket
  * For this method, Support will use an application that is already
    available to them for listing and downloading the files as needed.

## Escalations

Alliance Partners work closely with GitLab Support. To accommodate this, GitLab
Support provides all Alliance Partners with a personalized escalation form. When this
form is submitted, the system notifies an appropriate GitLab Support Manager
to help escalate a ticket the Alliance Partners have opened.
